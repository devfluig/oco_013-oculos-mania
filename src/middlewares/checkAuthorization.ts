import { Request, Response, NextFunction } from "express";

const token = <string>process.env.TOKEN;

export function checkAuthorization(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
      .status(400)
      .json({ error: "Missing authorization header parameter." });
  }

  if (authorization !== token) {
    return res
      .status(401)
      .json({ error: "Unauthorized. Invalid authorization code." });
  }

  return next();
}
