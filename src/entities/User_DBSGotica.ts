import { Entity, BaseEntity, Column, PrimaryColumn } from "typeorm";

@Entity({
  database: "DBSGotica",
  name: "MALADIRETANIVER_TAKE",
})
export class UserDBSGotica extends BaseEntity {
  @PrimaryColumn({
    type: "int",
    unique: true,
  })
  CODCLI: number;

  @Column({
    type: "varchar",
    length: 50,
    nullable: true,
  })
  NOMCLI: string;

  @Column({
    type: "varchar",
    length: 11,
    nullable: true,
  })
  CELULAR: string;

  @Column({
    type: "nvarchar",
    length: 4000,
    nullable: true,
  })
  DIAMES: string;

  @Column({
    type: "varchar",
    length: 50,
    nullable: true,
  })
  EMAIL: string;

  @Column({
    type: "varchar",
    length: 60,
    nullable: true,
  })
  NOMBAND: string;

  @Column({
    type: "int",
    unique: false,
    nullable: true,
  })
  CODLOJ: number
}
