import { Entity, BaseEntity, Column, PrimaryColumn } from "typeorm";

@Entity({
  database: "VolpeGoldenMix",
  name: "VW_ANIVERSARIANTESDIA",
})
export class UserVolpeGoldenMix extends BaseEntity {
  @PrimaryColumn({
    type: "int",
    unique: true,
  })
  PK_ID: number;

  @Column({
    type: "int",
    unique: true,
    nullable: true,
  })
  ID_SANKHYA: number;

  @Column({
    type: "varchar",
    length: 40,
    nullable: true,
  })
  DS_RAZAO: string;

  @Column({
    type: "varchar",
    length: 250,
    nullable: true,
  })
  DS_FONE: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  DS_EMAIL: string;

  @Column({
    type: "varchar",
    length: 30,
    nullable: true,
  })
  DS_BANDEIRA: string;

  @Column({
    type: "nvarchar",
    length: 4000,
    nullable: true,
  })
  DT_FUNDACAO: string;

  @Column({
    type: "int",
    unique: false,
    nullable: true,
  })
  FK_EMPRESA: any;
}
