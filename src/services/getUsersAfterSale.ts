import axios from "axios";
import { test } from "../schedules/apagar";

import { User } from "../types/user";

export async function getUsersAfterSale(store?: string) {
  const users: User[] = [];

  //logIn
  const resLogin = await axios({
    method: "post",
    url: "http://oculos.snk.ativy.com:40133/mge/service.sbr",
    params: {
      serviceName: "MobileLoginSP.login",
      outputType: "json",
    },
    data: {
      "serviceName": "MobileLoginSP.login",
      "requestBody": {
        "NOMUSU": {
          "$": "UPFLOW"
        },
        "INTERNO": {
          "$": "Trocar12345"
        },
        "KEEPCONNECTED": {
          "$": "S"
        }
      }
    },
  });

  const cookies = resLogin.headers["set-cookie"] as Array<string>;

  //check logIn
  const authCookie = cookies.find((cookie) => {
    return cookie.startsWith("JSESSIONID");
  });

  if (!authCookie) {
    throw new Error("It was not possible to authenticate on Shankya server.");
  }

  //get users
  const resQuery = await axios({
    method: "get",
    url: "http://oculos.snk.ativy.com:40133/mge/service.sbr",
    params: {
      serviceName: "DbExplorerSP.executeQuery",
      outputType: "json",
    },
    headers: {
      Cookie: authCookie,
    },
    data: {
      serviceName: "DbExplorerSP.executeQuery",
      requestBody: {
        sql: "SELECT * FROM POSVENDA_TAKE",
      },
    },
  });

  //logout
  const resLogOut = await axios({
    method: "post",
    url: "http://oculos.snk.ativy.com:40133/mge/service.sbr",
    params: {
      serviceName: "MobileLoginSP.logout",
      outputType: "json",
    },
    headers: {
      Cookie: authCookie,
    },
    data: {
      serviceName: "MobileLoginSP.logout",
      status: "1",
      pendingPrinting: "false",
    },
  });

  const contacts = resQuery.data?.responseBody?.rows as Array<any>;
  // const contacts = test.responseBody?.rows as Array<any>;  // Para teste.

  contacts.forEach((contact) => {
    const contactStore = contact[8] as string;

    if (store?.toUpperCase() === "MANIA") {
      if (contactStore?.toUpperCase()?.trim() === "OCULOS MANIA") {
        users.push({
          id: contact[1],
          name: contact[5],
          phone: contact[6]?.replace(/\D/g, "")
            ? contact[6]?.replace(/\D/g, "")
            : contact[6],
          store: contact[8] ? contact[8].trim() : 'MANIA',
          date: contact[9],
          database: "SHANKYA",
          nurel: contact[0],
          codeEmp: contact[3],
        });
      }
    } else if (store?.toUpperCase() === "GOLDEN") {
      if (contactStore?.toUpperCase()?.trim() === "GOLDEN MIX") {
        users.push({
          id: contact[1],
          name: contact[5],
          phone: contact[6]?.replace(/\D/g, "")
            ? contact[6]?.replace(/\D/g, "")
            : contact[6],
          store: contact[8] ? contact[8].trim() : 'GOLDEN MIX',
          date: contact[9],
          database: "SHANKYA",
          nurel: contact[0],
          codeEmp: contact[3],
        });
      }
    }
  });

  // console.log("users:", users);
  return users;
}
