import { getConnection } from "typeorm";

import { UserVolpeGoldenMix } from "../entities/User_VolpeGoldenMix";
import { UserDBSGotica } from "../entities/User_DBSGotica";
import { User } from "../types/user";

export async function getUsers(store?: string, database?: string) {
  let userVolpeGoldenMix: UserVolpeGoldenMix[] = [];
  let userDBSGotica: UserDBSGotica[] = [];

  let users: User[] = [];

  let auxVolpe: UserVolpeGoldenMix[] = []
  let auxGotica: UserDBSGotica[] = []

  switch (database?.toUpperCase()) {
    case "VOLPEGOLDENMIX":
      {
        const volpeGoldenMixConnection = getConnection("VolpeGoldenMix");

        if (store) {
          userVolpeGoldenMix = await volpeGoldenMixConnection.manager.find(
            UserVolpeGoldenMix,
            {
              where: { DS_BANDEIRA: store },
            }
          );

          auxVolpe = await volpeGoldenMixConnection.manager.find(
            UserVolpeGoldenMix,
            {
              where: { DS_BANDEIRA: 'GOLDEN' },
            }
          );

          userVolpeGoldenMix = userVolpeGoldenMix.concat(auxVolpe)

        } else {
          userVolpeGoldenMix = await volpeGoldenMixConnection.manager.find(
            UserVolpeGoldenMix
          );
        }

        userVolpeGoldenMix.forEach((user) => {
          const { PK_ID, DS_RAZAO, DS_FONE, DS_BANDEIRA, DT_FUNDACAO, FK_EMPRESA } = user;

          users.push({
            id: PK_ID,
            name: DS_RAZAO,
            phone: DS_FONE,
            store: DS_BANDEIRA,
            date: DT_FUNDACAO,
            database: "VOLPE_GOLDEN_MIX",
            codeEmp: Number(FK_EMPRESA.trim())
          });
        });
      }
      break;

    case "DBSGOTICA":
      {
        const dbsgOticaConnection = getConnection("DBSGotica");

        if (store) {
          userDBSGotica = await dbsgOticaConnection.manager.find(
            UserDBSGotica,
            {
              where: { NOMBAND: store },
            }
          );

          auxGotica = await dbsgOticaConnection.manager.find(
            UserDBSGotica,
            {
              where: { NOMBAND: 'GOLDEN' },
            }
          );

          userDBSGotica = userDBSGotica.concat(auxGotica)
        } else {
          userDBSGotica = await dbsgOticaConnection.manager.find(UserDBSGotica);
        }

        userDBSGotica.forEach((user) => {
          const { CODCLI, NOMCLI, CELULAR, NOMBAND, DIAMES, CODLOJ } = user;

          users.push({
            id: CODCLI,
            name: NOMCLI,
            phone: CELULAR,
            store: NOMBAND,
            date: DIAMES,
            database: "DBSG_OTICA",
            codeEmp: CODLOJ
          });
        });
      }
      break;

    default:
      {
        const volpeGoldenMixConnection = getConnection("VolpeGoldenMix");

        if (store) {
          userVolpeGoldenMix = await volpeGoldenMixConnection.manager.find(
            UserVolpeGoldenMix,
            {
              where: { DS_BANDEIRA: store },
            }
          );

          auxVolpe = await volpeGoldenMixConnection.manager.find(
            UserVolpeGoldenMix,
            {
              where: { DS_BANDEIRA: 'GOLDEN' },
            }
          );

          userVolpeGoldenMix = userVolpeGoldenMix.concat(auxVolpe)
        } else {
          userVolpeGoldenMix = await volpeGoldenMixConnection.manager.find(
            UserVolpeGoldenMix
          );
        }

        userVolpeGoldenMix.forEach((user) => {
          const { PK_ID, DS_RAZAO, DS_FONE, DS_BANDEIRA, DT_FUNDACAO, FK_EMPRESA } = user;

          users.push({
            id: PK_ID,
            name: DS_RAZAO,
            phone: DS_FONE,
            store: DS_BANDEIRA,
            date: DT_FUNDACAO,
            database: "VOLPE_GOLDEN_MIX",
            codeEmp: Number(FK_EMPRESA.trim())
          });
        });

        const dbsgOticaConnection = getConnection("DBSGotica");

        if (store) {
          userDBSGotica = await dbsgOticaConnection.manager.find(
            UserDBSGotica,
            {
              where: { NOMBAND: store },
            }
          );

          auxGotica = await dbsgOticaConnection.manager.find(
            UserDBSGotica,
            {
              where: { NOMBAND: 'GOLDEN' },
            }
          );
        } else {
          userDBSGotica = await dbsgOticaConnection.manager.find(UserDBSGotica);
        }

        userDBSGotica.forEach((user) => {
          const { CODCLI, NOMCLI, CELULAR, NOMBAND, DIAMES, CODLOJ } = user;

          users.push({
            id: CODCLI,
            name: NOMCLI,
            phone: CELULAR,
            store: NOMBAND,
            date: DIAMES,
            database: "DBSG_OTICA",
            codeEmp: CODLOJ
          });
        });
      }
      break;
  }

  return users;
}
