import fs from "fs";
import convert from "xml-js";
import axios from "axios";
import fetch from "node-fetch";

export function toTitleCase(str: string): string {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

export function getFirstName(fullName: string): string {
  const firstName = fullName.split(" ")[0];
  return toTitleCase(firstName);
}

export function validatePhoneNumber(phone: string) {
  //remove non-numeric values.
  phone = phone.replace(/\D/g, "");

  //check if it is a Brazilian phone number with starting with 55 and ddd
  if (phone.length === 13 || phone.length === 12) {
    if (phone.startsWith("55")) {
      return phone;
    } else {
      return false;
    }
  }

  //check if it is a Brazilian phone number with ddd
  if (phone.length === 10 || phone.length === 11) {
    return `55${phone}`;
  } else {
    return false;
  }
}

export function quickParsePhone(phone: string) {
  phone = phone.replace(/\D/g, "");

  if (phone.startsWith("55")) return phone;
  return `55${phone}`;
}

export function loadJSON(path: string, array: boolean = false) {
  try {
    const dataBuffer = fs.readFileSync(path);
    const dataJSON = dataBuffer.toString();
    return JSON.parse(dataJSON);
  } catch (e) {
    if (array) return [];
    return {};
  }
}

export function dateToday(GMT: number = -3) {
  const dataGMTBrasilia = new Date();
  const offset = (dataGMTBrasilia.getTimezoneOffset() + GMT * 60) * 60000;
  const today = dataGMTBrasilia.getTime();
  const final = new Date(today + offset);

  return final;
}

export function parse(num: string) {
  num = String(num);
  if (num.length < 2) num = "0" + num;
  return num;
}

export async function logoutSankhya(id: string) {

  let url = 'http://oculos.snk.ativy.com:40133/mge/service.sbr?serviceName=MobileLoginSP.logout'

  const response = await fetch(url, {
    method: 'POST',
    headers: { 'Cookie': `JSESSIONID=${id}` }
  }).then(async response => {
    if (await response.status != 200) {
      return {
        "success": false,
        "message-error": `Request failed with error code: ${response.status}`
      }
    }

    const xml = await response.text()
    const json = JSON.parse(convert.xml2json(xml, { compact: true, spaces: 2 }))

    let success = json?.serviceResponse?._attributes?.status

    if (success == 1) {
      return {
        success: true,
        data: {
          sessionId: id
        }
      }
    }

    return {
      success: false,
      "message-error": "Failed to log out: " + id
    }
  }).catch(async e => {
    return {
      success: false,
      'message-error': e
    }
  })

  return response
}

export async function loginSankhya() {
  let url = 'http://oculos.snk.ativy.com:40133/mge/service.sbr?serviceName=MobileLoginSP.login&outputType=json'

  var data = JSON.stringify({
    "serviceName": "MobileLoginSP.login",
    "requestBody": {
      "NOMUSU": {
        "$": 'UPFLOW'
      },
      "INTERNO": {
        "$": 'Trocar12345'
      },
      "KEEPCONNECTED": {
        "$": "S"
      }
    }
  });

  const response = await fetch(url, {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: data
  }).then(async response => {
    if (await response.status != 200) {
      return {
        "success": false,
        "message-error": `Request failed with error code: ${response.status}`
      }
    }

    const json = JSON.parse(await response.text())
    //const json = JSON.parse(convert.xml2json(xml, { compact: true, spaces: 2 }))

    const id = json?.responseBody?.jsessionid?.$ || false

    if (id == false) {
      return {
        "success": false,
        "message-error": "ID generation failed"
      }
    }

    return {
      "success": true,
      "data": {
        "sessionId": id
      }
    }
  }).catch(async e => {
    return {
      success: false,
      'message-error': e
    }
  })

  return response
}

export async function returnOfAfterSalesContacts(contacts: Array<any>) {
  try {

    let id: any = await loginSankhya()

    if (!id.success) throw new Error('Authentication token generation error.')

    id = id.data.sessionId

    let errors = 0
    let results = []

    for (let contact of contacts) {

      if (contact.Canal != 'WhatsApp') throw new Error('Channel is not whatsapp.')

      let retorno: any
      let store: any

      if (contact.Retorno == 'Enviado') retorno = '1'
      else if (contact.Retorno == 'Número Inválido') retorno = '2'
      else if (contact.Retorno == 'Sem WhatsApp') retorno = '3'
      else throw new Error('Return invalid.')

      if (['INDAIÁ', 'INDAIA'].indexOf(contact.Store.trim().toUpperCase()) !== -1) store = '1'
      else if (['MANIA', 'ÓCULOS MANIA', 'OCULOS MANIA'].indexOf(contact.Store.trim().toUpperCase()) !== -1) store = '4'
      else if (['GOLDEN', 'GOLDEN MIX', 'GOLDENMIX'].indexOf(contact.Store.trim().toUpperCase()) !== -1) store = '7'
      else throw new Error('Store invalid.')

      let data = JSON.stringify({
        "requestBody": {
          "dataSet": {
            "dataRow": {
              "localFields": {
                "DATA": {
                  "$": contact.Data
                },
                "NOME": {
                  "$": contact.Nome
                },
                "CANAL": {
                  "$": "1"
                },
                "CONTATO": {
                  "$": contact.Contato
                },
                "RETORNO": {
                  "$": retorno
                },
                "NUREL": {
                  "$": contact.Nurel
                },
                "BANDEIRA": {
                  "$": store
                },
                "CODEMP": {
                  "$": contact.CodeEmp
                }
              }
            },
            "rootEntity": "AD_RETORNOPOS",
            "includePresentationFields": "N",
            "entity": {
              "fieldset": {
                "list": ""
              }
            }
          }
        },
        "serviceName": "CRUDServiceProvider.saveRecord"
      });

      const result = await fetch('http://sankhya.rhfranquias.com.br:40133/mge/service.sbr?serviceName=CRUDServiceProvider.saveRecord&outputType=json', {
        method: "POST",
        headers: { 'Cookie': `JSESSIONID=${id}` },
        body: data
      })
        .then(async response => {
          if (response.status != 200) {
            return {
              "success": false,
              "message-error": `Request failed with error code: ${response.status}`
            }
          }

          let body = JSON.parse(await response.text())

          if (body.status != '1') {
            return {
              "success": false,
              "message-error": body.statusMessage ? body.statusMessage : `Request failed with error code: ${body.status}`
            }
          }

          return {
            "success": true,
            items: { nuret: body.responseBody?.entities?.entity?.NURET?.$ }
          }
        }).catch(err => {
          console.error(err)
          return {
            'success': false,
            'message-error': err
          }
        })

      if (!result.success) errors++

      results.push(result)
    }

    // for (let r of results) {
    //   if (!r.success) console.log('erro')
    // }

    // console.log(errors)

    return { errors: errors, results: results }

  } catch (e) {
    console.error(e);
    return {
      success: false,
      'message-error': e
    }
  }
}

export async function returnOfBirthdaysContacts(contacts: Array<any>) {
  try {

    let id: any = await loginSankhya()

    if (!id.success) throw new Error('Authentication token generation error.')

    id = id.data.sessionId

    let errors = 0
    let results = []

    for (let contact of contacts) {

      if (contact.Canal != 'WhatsApp') throw new Error('Channel is not whatsapp.')

      let retorno: any
      let store: any

      if (contact.Retorno == 'Enviado') retorno = '1'
      else if (contact.Retorno == 'Número Inválido') retorno = '2'
      else if (contact.Retorno == 'Sem WhatsApp') retorno = '3'
      else throw new Error('Return invalid.')

      if (['INDAIÁ', 'INDAIA'].indexOf(contact.Store.trim().toUpperCase()) !== -1) store = '1'
      else if (['MANIA', 'ÓCULOS MANIA', 'OCULOS MANIA'].indexOf(contact.Store.trim().toUpperCase()) !== -1) store = '4'
      else if (['GOLDEN', 'GOLDEN MIX', 'GOLDENMIX'].indexOf(contact.Store.trim().toUpperCase()) !== -1) store = '7'
      else throw new Error('Store invalid.')

      let data = JSON.stringify({
        "requestBody": {
          "dataSet": {
            "dataRow": {
              "localFields": {
                "DATA": {
                  "$": contact.Data
                },
                "NOME": {
                  "$": contact.Nome
                },
                "CANAL": {
                  "$": "1"
                },
                "CONTATO": {
                  "$": contact.Contato
                },
                "RETORNO": {
                  "$": retorno
                },
                "BANDEIRA": {
                  "$": store
                },
                "CODEMP": {
                  "$": contact.CodeEmp
                }
              }
            },
            "rootEntity": "AD_RETORNOANIVERSARIO",
            "includePresentationFields": "N",
            "entity": {
              "fieldset": {
                "list": ""
              }
            }
          }
        },
        "serviceName": "CRUDServiceProvider.saveRecord"
      });

      const result = await fetch('http://sankhya.rhfranquias.com.br:40133/mge/service.sbr?serviceName=CRUDServiceProvider.saveRecord&outputType=json', {
        method: "POST",
        headers: { 'Cookie': `JSESSIONID=${id}` },
        body: data
      })
        .then(async response => {
          if (response.status != 200) {
            return {
              "success": false,
              "message-error": `Request failed with error code: ${response.status}`
            }
          }

          let body = JSON.parse(await response.text())

          if (body.status != '1') {
            return {
              "success": false,
              "message-error": body.statusMessage ? body.statusMessage : `Request failed with error code: ${body.status}`
            }
          }

          return {
            "success": true,
            items: { nuret: body.responseBody?.entities?.entity?.NURET?.$ }
          }
        }).catch(err => {
          console.error(err)
          return {
            'success': false,
            'message-error': err
          }
        })

      if (!result.success) errors++

      results.push(result)
    }

    return { errors: errors, results: results }

  } catch (e) {
    console.error(e);
    return {
      success: false,
      'message-error': e
    }
  }
}

export function calculateDistance(userLat: number, userLong: number, partnerLat: number, partnerLong: number) {
  try {

    const pi = Math.PI

    const lat1Radianos = userLat * pi / 180
    const long1Radianos = userLong * pi / 180
    const lat2Radianos = partnerLat * pi / 180
    const long2Radianos = partnerLong * pi / 180

    return Math.acos(Math.cos(lat1Radianos) * Math.cos(long1Radianos) * Math.cos(lat2Radianos) * Math.cos(long2Radianos) + Math.cos(lat1Radianos) * Math.sin(long1Radianos) * Math.cos(lat2Radianos) * Math.sin(long2Radianos) + Math.sin(lat1Radianos) * Math.sin(lat2Radianos)) * 6371 * 1.15;

  } catch (e) {
    return false
  }
}

export function sortDistance(distA: any, distB: any) {
  if (distA.dist > distB.dist) return 1
  if (distA.dist < distB.dist) return -1

  return 0
}

export function formatString(str: string) {
  if (str && str != "undefined") {

    str = str.toLowerCase()
    let arrayStr = str.split(' ')

    let strAux = ''

    arrayStr.forEach(word => {
      let firstLetter = word[0].toUpperCase()
      strAux += firstLetter + word.slice(1,) + ' '
    });

    return strAux.slice(0, -1)

  }

  return str
}

export function formatStringServiceStations(stores: any) {

  if (stores) {
    let str = ''
    let boldStart = 'boldStart'
    let boldEnd = 'boldEnd'

    for (let i = 0; i < stores.length; i++) {
      str += `${boldStart}${stores[i]['flag']}${boldEnd}\n`
      str += `${boldStart}${stores[i]['name']}${boldEnd}\n`
      str += `${boldStart}Endereço:${boldEnd} ${stores[i]['street']}, nº ${stores[i]['number']}, bairro ${stores[i]['district']}, ${stores[i]['city']}\n`

      if (stores[i]['phoneNumber'] && stores[i]['phoneNumber'] != 'undefined') {
        str += `${boldStart}Telefone:${boldEnd} ${stores[i]['phoneNumber']}\n`
      }

      if (stores[i]['email'] && stores[i]['email'] != 'undefined') {
        str += `${boldStart}E-mail:${boldEnd} ${stores[i]['email']}\n`
      }

      // if (stores[i]['phoneNumber'].length == 1)
      //   tels += stores[i]['phoneNumber']
      // else {
      //   for (let j = 0; j < stores[i]['phoneNumber'].length; j++) {
      //     tels += stores[i]['phoneNumber'][j]
      //     tels += ', '
      //   }
      //   tels = tels.slice(0, -2)
      // }

      // str += `${boldStart}Telefone:${boldEnd} ${tels}\n`
      // str += `${boldStart}Horário de atendimento:${boldEnd} ${stores[i]['Horario']}\n`
      str += `Se preferir aqui vai o Link da localização no Google: https://maps.google.com/?q=${stores[i]['latitude']},${stores[i]['longitude']}`
      str += `\n\n`
    }

    return str
  }

  return {
    success: false,
    "message-error": "No service stations."
  }
}