import express from "express";

import { checkAuthorization } from "../middlewares/checkAuthorization";
import { schedules } from "../schedules/SchedulesManager";

const router = express.Router();

router.post("/api/schedules", checkAuthorization, async (req, res) => {
  if (schedules.areSchedulesActive) {
    schedules.stopSchedules();
    res.send({
      status: false,
      message: "Schedules successfully stopped",
    });

    console.log("Schedules stopped.");
    //
  } else {
    schedules.startSchedules();
    res.send({
      status: true,
      message: "Schedules successfully started",
    });

    console.log("Schedules started.");
  }
});

router.get("/api/schedules", checkAuthorization, async (req, res) => {
  if (schedules.areSchedulesActive) {
    res.send({
      status: true,
      message: `Active running schedules: ${schedules.scheduleNames}`,
    });
  } else {
    res.send({
      status: false,
      message: "There are no active schedules running",
    });
  }
});

export { router as schedules };
