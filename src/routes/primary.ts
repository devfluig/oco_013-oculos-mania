import express from "express";

const router = express.Router();

router.get("/", (req, res) => {
  res.status(200).json({
    NAME: "API for integration between RH Franquia systems and Take Blip v1.0.2",
  });
});

export { router as primary };
