import express from "express";

import { checkAuthorization } from "../middlewares/checkAuthorization";
import { getUsers } from "../services/getUsers";

import { getUsersAfterSale } from "../services/getUsersAfterSale";

const router = express.Router();

router.get("/api/user", checkAuthorization, async (req, res) => {
  const store = req.headers.store as string;
  const database = req.headers.database as string;

  try {
    const data = await getUsers(store, database);
    res.send(data);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.get("/api/userShankya", checkAuthorization, async (req, res) => {
  const store = req.headers.store as string;

  try {
    const data = await getUsersAfterSale(store);
    res.send(data);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

export { router as user };
