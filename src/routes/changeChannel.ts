import express from "express";
import path from 'path'
import fs from 'fs'

import { checkAuthorization } from "../middlewares/checkAuthorization";
import { loadJSON } from "../utils/functions"

const router = express.Router();

router.put("/api/channel", checkAuthorization, (req, res) => {
  const channel = req.body.channel as string
  if(!['whatsapp', 'sms', 'email'].includes(channel.toLowerCase()))
    return res.status(400).send({ message: "Invalid channel type. Available channel types are: whatsapp, sms, email."})

  try {
    const data = loadJSON(path.join(__dirname, '..', 'schedules', 'method.json'))
    fs.writeFileSync(path.join(__dirname, '..', 'schedules', 'method.json'), JSON.stringify({ channel: channel.toLowerCase() }))
    console.log(path.join(__dirname, '..', 'schedules', 'method.json'))

    return res.status(201).send({
        message: 'Channel updated successfully: ' + channel
    });
  } catch (error) {
    return res.status(500).json({ status: 500, error: error });
  }
})

export { router as channel };
