import express from "express"
import { checkAuthorization } from "../middlewares/checkAuthorization"
import { calculateDistance, sortDistance, loginSankhya, logoutSankhya, formatString } from "../utils/functions"
import convert from "xml-js";
import fetch from "node-fetch"

const router = express.Router()

const loginRoyal = String(process.env.LOGINROYAL)
const interno2 = String(process.env.INTERNO2)
const urlSankhya = process.env.URL_SANKHYA

router.post("/api/searchStores/:store", checkAuthorization, async (req, res) => {

  try {

    const { locationObject } = req.body
    let store = req.params.store == 'golden' ? 'VW_LOJASGOLDEN' : req.params.store == 'mania' ? 'VW_LOJASMANIA' : null

    if (!locationObject) throw new Error('LocationObject is missing.')

    if (!locationObject.latitude || !locationObject.longitude) throw new Error('Latitude and longitude is required.')

    if (!store) throw new Error('Store is missing.')

    let response = await loginSankhya()
      .then(async (response: any) => {

        if (response.success) {
          let id = response.data?.sessionId
          console.log("Login succeeded: " + id)

          let url = urlSankhya + '=CRUDServiceProvider.loadView'
          let data = `<?xml version="1.0" encoding="UTF-8"?>\r\n<serviceRequest serviceName="CRUDServiceProvider.loadView">\r\n    <requestBody>\r\n        <query viewName="${store}" orderBy=""></query>\r\n    </requestBody>\r\n</serviceRequest>`

          const result: any = await fetch(url, {
            method: "POST",
            headers: { 'Content-Type': 'text/xml; charset=utf-8', 'Cookie': `JSESSIONID=${id}`, 'Accept-Encoding': 'UTF-8' },
            body: data
          }).then(async response => {
            if (await response.status != 200) {
              return {
                "success": false,
                "message-error": `Request failed with error code: ${response.status}`
              }
            }

            const xml = await response.text()

            const json = JSON.parse(convert.xml2json(xml, { compact: true, spaces: 2 }))

            let partners = json?.serviceResponse?.responseBody?.records?.record || false

            let partnersList = []
            let partnersDist = []

            if (partners) {

              const userLat = parseFloat(locationObject["latitude"])
              const userLong = parseFloat(locationObject["longitude"])

              if (isNaN(userLat) || isNaN(userLong) || !isFinite(userLat) || !isFinite(userLong)) {
                return {
                  success: false,
                  "message-error": "Invalid latitude or longitude"
                }
              }

              for (let i = 0; i < partners.length; i++) {

                let partnerLat = parseFloat(partners[i]?.LATITUDE?._cdata)
                let partnerLong = parseFloat(partners[i]?.LONGITUDE?._cdata)

                if (isNaN(partnerLat) || isNaN(partnerLong) || !isFinite(partnerLat) || !isFinite(partnerLong)) {
                  continue
                }

                let dist = calculateDistance(userLat, userLong, partnerLat, partnerLong)

                if (dist && dist <= 25) {
                  partnersDist.push({ "index": i, "dist": dist })
                }
              }

              partnersDist = partnersDist.sort(sortDistance)

              let j = 0

              for (let dist of partnersDist) {
                //if (j == 5) break
                partnersList.push({
                  flag: String(partners[dist.index]?.BANDEIRA?._cdata).trim(),
                  name: formatString(String(partners[dist.index]?.NOMEFANTASIA?._cdata).trim()),
                  street: formatString(String(partners[dist.index]?.NOMEEND?._cdata).trim()),
                  number: String(partners[dist.index]?.NUMEND?._cdata).trim(),
                  district: formatString(String(partners[dist.index]?.NOMEBAI?._cdata).trim()),
                  city: formatString(String(partners[dist.index]?.NOMECID?._cdata).trim()),
                  phoneNumber: String(partners[dist.index]?.TELEFONE._cdata).trim(),
                  email: String(partners[dist.index]?.EMAIL?._cdata).trim(),
                  latitude: String(partners[dist.index]?.LATITUDE._cdata).trim(),
                  longitude: String(partners[dist.index]?.LONGITUDE._cdata).trim()
                })
                j++
              }
            }

            if (partnersList.length == 0) {
              return {
                success: false,
                'message-error': 'No partner found'
              }
            }

            //let str = formatStringServiceStations(partnersList)

            return {
              success: true,
              data: {
                //"string": str,
                "stores": partnersList
              }
            }

          }).catch(async e => {
            console.error(e)
            return {
              success: false,
              'message-error': e
            }
          })

          if (!result.success) throw new Error('Service stations not found.')

          logoutSankhya(id)

          return {
            success: true,
            data: result.data
          }
        }

        throw new Error('Authentication error in Sankhya.')

      }).catch(async e => {
        console.error(e)
        return {
          success: false,
          'message-error': e
        }
      })

    if (response.success) return res.send(response)

    return res.status(500).send(response)


  } catch (err) {
    console.log(err.message)
    return res.status(400).send({
      success: false,
      "message-error": err.message
    })
  }

})

export { router as searchStores }