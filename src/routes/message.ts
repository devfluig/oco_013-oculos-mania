import express from "express";
import { checkAuthorization } from "../middlewares/checkAuthorization";
import { BlipContact } from "../blip/BlipContact";

const router = express.Router();

router.post("/api/message", checkAuthorization, async (req, res) => {
  const {
    botKey,
    whatsAppNumber,
    botId,
    flowId,
    blockId,
    messageTemplateName,
    messageNameSpace,
    messageParameters,
  } = req.body;

  if (
    !botKey ||
    !whatsAppNumber ||
    !botId ||
    !flowId ||
    !blockId ||
    !messageTemplateName ||
    !messageNameSpace ||
    !messageParameters
  ) {
    res.status(400).send({
      error:
        "Bad request. Missing one of the required parameters: botKey, whatsAppNumber, botId, flowId, blockId, messageTemplateName, messageNameSpace, messageParameters.",
    });
    return;
  }

  try {
    const blipContact = new BlipContact(botKey, whatsAppNumber);

    const resSendMessage = await blipContact.sendMessageTemplateWithRedirection(
      botId,
      flowId,
      blockId,
      messageTemplateName,
      messageNameSpace,
      messageParameters
    );

    res.send(resSendMessage);
    return;
    //
  } catch (error) {
    res.status(500).json({ error: error?.message });
    return;
  }
});

export { router as message };
