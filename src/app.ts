import express from "express";
import swaggerUi from "swagger-ui-express";

import { createConnections } from "typeorm";
import { UserDBSGotica } from "./entities/User_DBSGotica";
import { UserVolpeGoldenMix } from "./entities/User_VolpeGoldenMix";

import swaggerFile from "./swagger.json";
import { primary } from "./routes/primary";
import { user } from "./routes/user";
import { message } from "./routes/message";
import { schedules } from "./routes/schedules";
import { channel } from "./routes/changeChannel";
import { searchStores } from "./routes/searchStores";
// import { sendAfterSaleMessage } from "./schedules/sendAfterSaleMessageDaily";

/**
 * @todo INCLUIR O TEMPLATE E NAMESPACE DA MENSAGEM ATIVA DE PÓS-VENDA NO .ENV
 */

const port = process.env.PORT || 3000; // Port where the application will run.

const app = express();

const main = async () => {
  try {
    await createConnections([
      {
        name: "VolpeGoldenMix",
        type: "mssql",
        host: "bdativy.rhfranquias.com.br",
        port: 65200,
        username: "take",
        password: "t4k3$us3r",
        database: "VolpeGoldenMix",
        entities: [UserVolpeGoldenMix],
        options: {
          encrypt: false,
          readOnlyIntent: true,
        },
      },
      {
        name: "DBSGotica",
        type: "mssql",
        host: "131.161.123.138",
        port: 31433,
        username: "take",
        password: "t4k3$us3r",
        database: "DBSGotica",
        entities: [UserDBSGotica],
        options: {
          encrypt: false,
          readOnlyIntent: true,
        },
      },
    ]);

    console.log("Connected to SQL Servers");
    // console.log(sendAfterSaleMessage());

    app.use(express.json());

    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));
    app.use(primary);
    app.use(user);
    app.use(message);
    app.use(schedules);
    app.use(channel);
    app.use(searchStores);

    app.listen(port, () => {
      console.log(`🔥 Server succesfully running on port ${port}`);
    });
  } catch (error) {
    console.error(error);
    throw new Error("Unable to connect to SQL Server");
  }
};

main();
