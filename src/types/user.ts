export interface User {
  id: number;
  name: string;
  phone: string;
  store: string;
  date: string;
  database: string;
  email?: string;
  nurel?: string;
  codeEmp?: any
}
