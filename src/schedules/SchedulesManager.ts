import cron from "node-cron";

import { sendBirthMessageDaily } from "./sendBirhtdayMessageDaily";
import { sendAfterSaleMessageDaily } from "./sendAfterSaleMessageDaily";
import { sendBirthMessageDailyGolden } from './sendBirhtdayMessageDailyGolden'
import { sendAfterSaleMessageDailyGolden } from "./sendAfterSaleMessageDailyGolden"

class SchedulesManager {
  private _schedules: Array<cron.ScheduledTask>;
  private _scheduleNames: Array<string>;
  private _areSchedulesActive: boolean;

  constructor(
    scheduleNames: Array<string>,
    schedules: Array<cron.ScheduledTask>
  ) {
    this._areSchedulesActive = false;
    this._scheduleNames = scheduleNames;
    this._schedules = schedules;
  }

  get schedules() {
    return this._schedules;
  }

  get scheduleNames() {
    return this._scheduleNames;
  }

  get areSchedulesActive() {
    return this._areSchedulesActive;
  }

  startSchedules() {
    this._schedules.forEach((schedule) => {
      schedule.start();
    });

    this._areSchedulesActive = true;
  }

  stopSchedules() {
    this._schedules.forEach((schedule) => {
      schedule.stop();
    });

    this._areSchedulesActive = false;
  }
}

const schedules = new SchedulesManager(
  ["sendBirthMessageDaily", "sendAfterSaleMessageDaily", "sendBirthMessageDailyGolden", "sendAfterSaleMessageDailyGolden"],
  [sendBirthMessageDaily, sendAfterSaleMessageDaily, sendBirthMessageDailyGolden, sendAfterSaleMessageDailyGolden]
);

export { schedules };
