import cron from "node-cron";
import path from "path";
import { User } from "src/types/user";
import { BlipContact } from "../blip/BlipContact";
import { getUsersAfterSale } from "../services/getUsersAfterSale";
import { quickParsePhone, getFirstName, loadJSON, returnOfAfterSalesContacts } from "../utils/functions";

const store = 'GOLDEN';
const ROUTER_KEY = process.env.BOT_KEY_GOLDEN ?? "";
const TEMPLATE = 'pos_venda_v2';
const NAMESPACE = '48bc6881_e337_4d3b_a851_225289cf7cac';
const botId = "mensagemativaprd1";
const flowId = "aaf00f70-a411-42e7-9d73-05152d226cb5";
const blockId = "eaabfb0a-aa54-483f-ac34-baab1cd2b890"; // "[PÓS-VENDA] - Início msg ativa"

import { validatePhoneNumber } from "../utils/functions";

function uniqByKeepLast(data: Array<Object>, key: Function) {
  return [...new Map(data.map((x) => [key(x), x])).values()];
}

const { dateToday, parse } = require("../utils/functions");

async function sendAfterSaleMessageGolden() {
  function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  console.log("Sending daily AfterSale Message");

  try {
    var contacts = await getUsersAfterSale(store);

    console.log("Quantidade de contatos Pós Venda: ", contacts.length);

    const channel = loadJSON(path.join(__dirname, "method.json")).channel;

    // /**
    //  * Exemplo de retorno de 'contacts':
    //  * [
    //     {
    //       id: 18695,
    //       name: 'RITA DE CASSIA DE OLIVEIRA DOS SANTOS',
    //       phone: '11914815663',
    //       store: 'MANIA',
    //       date: '09112021 00:00:00',
    //       database: 'SHANKYA'
    //     },
    //     {
    //       ...
    //     }
    //   ]
    //  */

    if (contacts.length > 0) {
      contacts = uniqByKeepLast(
        contacts,
        (it: { name: string }) => it.name
      ) as User[];

      console.log(
        `Sending After Sale Messages to ${contacts.length} contacts..`
      );

      let rows = [] as Array<Object>;
      const today = dateToday();
      let date = `${parse(today.getDate())}/${parse(
        today.getMonth() + 1
      )}/${today.getFullYear()}`;

      var enviados = 0;
      var invalido = 0;
      var blip = 0;

      contacts.map(async (person) => {

        const phone =
          person.phone[0] == "0"
            ? validatePhoneNumber(person.phone.substring(1))
            : validatePhoneNumber(person.phone);

        //const phone = "553198078052"
        if (!phone) {
          console.log(`Número Inválido: ${person.name} - ${person.phone}`);
          invalido += 1;
          rows.push({
            Data: date,
            Nome: person.name,
            Canal: "WhatsApp",
            "Contato": person.phone,
            "Tipo de Mensagem": "Pós-Venda",
            Retorno: "Número Inválido",
            Store: person.store,
            Nurel: person.nurel,
            CodeEmp: person.codeEmp
          });
        } else {
          const user = new BlipContact(ROUTER_KEY, quickParsePhone(phone));
          const email = person.email; // Verificar se a view do banco de dados volta o email da pessoa.

          const response = await user.sendMessageTemplateWithRedirection(
            botId,
            flowId,
            blockId,
            TEMPLATE,
            NAMESPACE,
            [
              {
                // A mensagem ativa deve possuir a mesma quantidade de parâmetros passadas aqui.
                type: "text",
                text: getFirstName(person.name),
              },
            ],
            channel,
            email
          );

          // let response = { error: false }

          // if (Math.random() <= 0.3) response.error = true

          if (response?.error) {
            console.log(`Número Inexistente: ${person.name} - ${person.phone}`);
            rows.push({
              Data: date,
              Nome: person.name,
              Canal: "WhatsApp",
              "Contato": person.phone,
              "Tipo de Mensagem": "Pós-Venda",
              Retorno: "Sem WhatsApp",
              Store: person.store,
              Nurel: person.nurel,
              CodeEmp: person.codeEmp
            });
            blip += 1;
          } else {
            rows.push({
              Data: date,
              Nome: person.name,
              Canal: "WhatsApp",
              "Contato": person.phone,
              "Tipo de Mensagem": "Pós-Venda",
              Retorno: "Enviado",
              Store: person.store,
              Nurel: person.nurel,
              CodeEmp: person.codeEmp
            });
            enviados += 1;
            console.log(`Número Válido: ${person.name} - ${phone}`);
          }
        }
      });
      await sleep(5000);
      console.log("Enviados: ", enviados);
      console.log("Inválidos: ", invalido);
      console.log("Inexistentes: ", blip);

      let { errors, results }: any = await returnOfAfterSalesContacts(rows)

      if (errors == 0) console.log('Dados gravados no Sankhya com sucesso.')
      else {
        console.log('Erro na gravação dos dados no Sankhya.')
        for (let result of results) {
          if (!result.success) {
            console.log(result['message-error'])
          }
        }
      }

    } else console.log("After Sale Message: No users were found.");
  } catch (error) {
    console.log(error);
  }
}

const sendAfterSaleMessageDailyGolden = cron.schedule(
  "0 00 13 * * *",
  sendAfterSaleMessageGolden,
  {
    scheduled: true,
    timezone: "America/Sao_Paulo",
  }
);

export { sendAfterSaleMessageDailyGolden, sendAfterSaleMessageGolden };
