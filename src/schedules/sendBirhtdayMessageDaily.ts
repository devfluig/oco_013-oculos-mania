import cron from "node-cron";
import path from "path";
import { getUsers } from "../services/getUsers";
import { BlipContact } from "../blip/BlipContact";

import {
  getFirstName,
  validatePhoneNumber,
  loadJSON,
  returnOfBirthdaysContacts
} from "../utils/functions";

const store = process.env.STORE as string;

const botKey = process.env.BOT_KEY as string;
const botId = "oculosmaniamsgsativashmg";
const flowId = "b3a263e4-0eac-4a1b-87d6-f43ad28f2508";
// const blockId = "06b7ea79-0e74-4646-9826-5e2d2edad36d";
// const blockId = "8068f73b-7c34-489b-ad82-7a59e8613b27";
const blockId = "f96a91b6-9202-4578-9333-1f77f1e85d32";
// const messageTemplateName = "aniversario";
// const messageNameSpace = "8763732b_120a_465d_9523_df781d0e7bf8";
const messageTemplateName = "aniversario_v2";
const messageNameSpace = "8763732b_120a_465d_9523_df781d0e7bf8";
// const messageParameters = [
//   {
//     type: "text",
//     text: "",
//   },
// ];

const { dateToday, parse } = require("../utils/functions");

async function sendBirthMessage() {
  function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  console.log("Sending daily Birthday Message");

  try {
    //get users from database
    const contacts = await getUsers(store);

    console.log("Quantidade Aniversário: ", contacts.length);

    const channel = loadJSON(path.join(__dirname, "method.json")).channel;

    console.log(`Quantidades de contatos: ${contacts.length}`);
    // console.log("contacts:", contacts);

    let rows = [] as Array<Object>;
    const today = dateToday();
    let date = `${parse(today.getDate())}/${parse(
      today.getMonth() + 1
    )}/${today.getFullYear()}`;

    var enviados = 0;
    var invalido = 0;
    var blip = 0;
    //send message to each user

    contacts.forEach(async (contact) => {

      const phone =
        contact.phone[0] == "0"
          ? validatePhoneNumber(contact.phone.substring(1))
          : validatePhoneNumber(contact.phone);


      if (!phone) {
        console.log(`Número Inválido: ${contact.name} - ${contact.phone}`);
        invalido += 1;
        rows.push({
          Data: date,
          Nome: contact.name,
          Canal: "WhatsApp",
          "Contato": contact.phone,
          "Tipo de Mensagem": "Aniversário",
          Retorno: "Número Inválido",
          Store: contact.store,
          CodeEmp: contact.codeEmp
        });
      } else {
        const contactName = getFirstName(contact.name);
        const email = contact.email; // Verificar se a view do banco de dados volta o email da pessoa.
        // messageParameters[0].text = contactName;
        let messageParameters = [
          {
            type: "text",
            text: contactName,
          },
        ];
        const blipContact = new BlipContact(botKey, phone);

        const response = await blipContact.sendMessageTemplateWithRedirection(
          botId,
          flowId,
          blockId,
          messageTemplateName,
          messageNameSpace,
          messageParameters,
          channel,
          email,
          "Feliz aniversário!"
        );

        // let response = { error: false }

        // if (Math.random() <= 0.3) response.error = true

        if (response?.error) {
          console.log(`Número Inexistente: ${contactName} - ${phone}`);
          rows.push({
            Data: date,
            Nome: contact.name,
            Canal: "WhatsApp",
            "Contato": contact.phone,
            "Tipo de Mensagem": "Aniversário",
            Retorno: "Sem WhatsApp",
            Store: contact.store,
            CodeEmp: contact.codeEmp
          });
          blip += 1;
        } else {
          rows.push({
            Data: date,
            Nome: contact.name,
            Canal: "WhatsApp",
            "Contato": contact.phone,
            "Tipo de Mensagem": "Aniversário",
            Retorno: "Enviado",
            Store: contact.store,
            CodeEmp: contact.codeEmp
          });
          enviados += 1;
          console.log(`Número Válido: ${contactName} - ${phone}`);
        }
      }
    });
    await sleep(5000);
    console.log("Enviados: ", enviados);
    console.log("Inválidos: ", invalido);
    console.log("Inexistentes: ", blip);

    let { errors, results }: any = await returnOfBirthdaysContacts(rows)

    if (errors == 0) console.log('Dados gravados no Sankhya com sucesso.')
    else {
      console.log('Erro na gravação dos dados no Sankhya.')
      for (let result of results) {
        if (!result.success) {
          console.log(result['message-error'])
        }
      }
    }

  } catch (error) {
    console.log(error);
  }
}

// let aux = sendBirthMessage();
let time = "00 00 10 * * *";
const sendBirthMessageDaily = cron.schedule(`${time}`, sendBirthMessage, {
  scheduled: true,
  timezone: "America/Sao_Paulo",
});

export { sendBirthMessageDaily };
